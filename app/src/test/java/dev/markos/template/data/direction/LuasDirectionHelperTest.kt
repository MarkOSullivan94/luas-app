package dev.markos.template.data.direction

import org.junit.Assert.assertTrue
import org.junit.Test

/**
 *
 */
class LuasDirectionHelperTest {
    @Test
    fun showing_correctly_as_outbound() {
        // Wednesday, 18 November 2020 02:11:04.218
        val time1 = 1605665464218L
        val direction1 = DefaultLuasDirectionHelper.getLuasDirection(time1)

        // Wednesday, 18 November 2020 10:22:32.193
        val time2 = 1605694952193L
        val direction2 = DefaultLuasDirectionHelper.getLuasDirection(time2)

        assertTrue(direction1 == LuasDirection.OUTBOUND)
        assertTrue(direction2 == LuasDirection.OUTBOUND)
    }

    @Test
    fun showing_correctly_as_inbound() {
        // Tuesday, 17 November 2020 20:40:18
        val time1 = 1605645618000L
        val direction1 = DefaultLuasDirectionHelper.getLuasDirection(time1)

        // Tuesday, 17 November 2020 23:05:18
        val time2 = 1605654318000L
        val direction2 = DefaultLuasDirectionHelper.getLuasDirection(time2)

        assertTrue(direction1 == LuasDirection.INBOUND)
        assertTrue(direction2 == LuasDirection.INBOUND)
    }
}