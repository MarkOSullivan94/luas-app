package dev.markos.template.data.datahelper

import dev.markos.template.data.direction.LuasDirection
import dev.markos.template.model.Direction
import dev.markos.template.model.StopInfo
import dev.markos.template.model.Tram
import org.junit.Assert.assertTrue
import org.junit.Test

class DataHelperTest {

    private val tramsInbound = listOf(
        Tram(
            dueMins = "9",
            destination = "Sandyford"
        ),
        Tram(
            dueMins = "20",
            destination = "Sandyford"
        )
    )

    private val tramsOutbound = listOf(
        Tram(
            dueMins = "3",
            destination = "Bride's Glen"
        ),
        Tram(
            dueMins = "22",
            destination = "Bride's Glen"
        )
    )

    private val directions = listOf(
        Direction(
            name = "Inbound",
            trams = tramsInbound
        ),
        Direction(
            "Outbound",
            trams = tramsOutbound
        )
    )

    private val data = StopInfo(
        createdDate = "2020-11-18T09:50:16",
        stop = "Marlborough",
        stopAbv = "MAR",
        directions = directions
    )

    @Test
    fun outbound_does_not_show_inbound_direction_data() {
        val newData = DataHelper.removeExtraDirectionFromApiResponse(data, LuasDirection.OUTBOUND)
        assertTrue(newData.directions.first().name.toLowerCase() == "outbound")
    }

    @Test
    fun new_data_only_shows_one_direction() {
        val newData = DataHelper.removeExtraDirectionFromApiResponse(data, LuasDirection.OUTBOUND)
        assertTrue(newData.directions.size == 1)
    }
}