package dev.markos.template.data.viewstate

import dev.markos.template.model.LuasData


sealed class LuasViewState {
    data class Ready(val data: LuasData) : LuasViewState()
    object Error : LuasViewState()
}
