package dev.markos.template.data.datahelper

import android.annotation.SuppressLint
import dev.markos.template.data.direction.LuasDirection
import dev.markos.template.model.LuasData
import dev.markos.template.model.StopInfo

abstract class BaseDataHelper {

    /**
     * API returns both inbound & outbound directions - we do not need both so this
     * function removes the unneeded direction data
     */
    fun removeExtraDirectionFromApiResponse(
        apiResponse: StopInfo,
        directionToKeep: LuasDirection
    ): StopInfo {
        return removeExtraDirectionFromApiResponse(apiResponse, directionToKeep.toString())
    }

    @SuppressLint("DefaultLocale")
    private fun removeExtraDirectionFromApiResponse(
        apiResponse: StopInfo,
        directionNameToKeep: String
    ): StopInfo {
        val direction =
            apiResponse.directions.filter { it.name.toLowerCase() == directionNameToKeep.toLowerCase() }

        return apiResponse.copy(
            directions = direction
        )
    }

    abstract fun convert(apiResponse: StopInfo, directionToKeep: LuasDirection): LuasData
}