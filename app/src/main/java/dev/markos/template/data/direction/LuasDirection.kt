package dev.markos.template.data.direction

enum class LuasDirection {
    INBOUND,
    OUTBOUND
}