package dev.markos.template.data.direction

import java.text.SimpleDateFormat
import java.util.*

/**
 * Extending base [LuasDirectionHelper] as core logic could change on special circumstances
 * E.g. Perhaps in future HolidayLuasDirectionHelper may be needed
 */
object DefaultLuasDirectionHelper : LuasDirectionHelper {

    private const val TIME_FORMAT = "HH:mm:ss"

    override fun getLuasDirection(time: Long): LuasDirection {
        return if (isOutbound(time)) {
            LuasDirection.OUTBOUND
        } else if (isInbound(time)) {
            LuasDirection.INBOUND
        } else {
            throw IllegalArgumentException("Cannot be neither outbound or inbound direction")
        }
    }

    private fun isOutbound(time: Long): Boolean {

        return isTimeBetween(
            currentTime = time,
            startTime = "00:00:00",
            finishTime = "12:00:00"
        )
    }

    private fun isInbound(time: Long): Boolean {
        return isTimeBetween(
            currentTime = time,
            startTime = "12:00:01",
            finishTime = "23:59:59"
        )
    }

    private fun isTimeBetween(
        currentTime: Long,
        startTime: String,
        finishTime: String
    ): Boolean {
        val dateFormat = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
        val afterCalendar = Calendar.getInstance().apply {
            time = dateFormat.parse(startTime)
            add(Calendar.DATE, 1)
        }
        val beforeCalendar = Calendar.getInstance().apply {
            time = dateFormat.parse(finishTime)
            add(Calendar.DATE, 1)
        }

        val current = Calendar.getInstance().apply {
            val localTime = dateFormat.format(currentTime)
            time = dateFormat.parse(localTime)
            add(Calendar.DATE, 1)
        }
        return current.time.after(afterCalendar.time) && current.time.before(beforeCalendar.time)
    }
}