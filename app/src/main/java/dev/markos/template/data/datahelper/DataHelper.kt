package dev.markos.template.data.datahelper

import dev.markos.template.data.direction.LuasDirection
import dev.markos.template.model.LuasData
import dev.markos.template.model.StopInfo

object DataHelper : BaseDataHelper() {

    /**
     * All we care about
     */
    override fun convert(apiResponse: StopInfo, direction: LuasDirection): LuasData {
        val data = super.removeExtraDirectionFromApiResponse(
            apiResponse = apiResponse,
            directionToKeep = direction
        )
        return convert(data)
    }

    private fun convert(data: StopInfo): LuasData {
        // should only ever be one direction here so fine to use first
        return LuasData(data.directions.first().trams)
    }
}