package dev.markos.template.data

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dev.markos.template.data.direction.LuasDirectionHelper
import dev.markos.template.data.viewstate.LuasViewState
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.time.Instant

class LuasViewModel @ViewModelInject constructor(
    private val luasRepository: LuasRepository,
    private val directionHelper: LuasDirectionHelper
) : BaseViewModel() {

    init {
        updateForecast()
    }

    private val _luasForecast = MutableLiveData<LuasViewState>()

    fun updateForecast() {
        val direction = directionHelper.getLuasDirection(Instant.now().epochSecond)
        luasRepository.fetchForecast(direction)
            .subscribeBy(
                onSuccess = { _luasForecast.value = LuasViewState.Ready(it) },
                onError = {
                    Log.e(TAG, "updateForecast:", it)
                    _luasForecast.value = LuasViewState.Error
                }
            )
            .addTo(disposibles)
    }

    val luasForecast: LiveData<LuasViewState>
        get() = _luasForecast
}



