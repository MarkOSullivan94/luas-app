package dev.markos.template.data.direction

interface LuasDirectionHelper {
    fun getLuasDirection(time: Long): LuasDirection
}

