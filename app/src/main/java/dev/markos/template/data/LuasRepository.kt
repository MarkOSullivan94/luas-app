package dev.markos.template.data

import android.util.Log
import dev.markos.template.data.api.LuasService
import dev.markos.template.data.datahelper.BaseDataHelper
import dev.markos.template.data.direction.LuasDirection
import dev.markos.template.model.LuasData
import dev.markos.template.model.StopInfo
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LuasRepository @Inject constructor(
    private val luasService: LuasService,
    private val dataHelper: BaseDataHelper
) {

    private val TAG = this::class.java.simpleName

    fun fetchForecast(luasDirection: LuasDirection): Single<LuasData> {
        return when (luasDirection) {
            LuasDirection.OUTBOUND -> fetchOutboundForecast()
            LuasDirection.INBOUND -> fetchInboundForecast()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                Log.d(TAG, "getStopInfo: $it")
                dataHelper.convert(it, luasDirection)
            }

    }

    private fun fetchOutboundForecast(): Single<StopInfo> {
        return luasService.fetchMarlboroughTowardsOutbound()
    }

    private fun fetchInboundForecast(): Single<StopInfo> {
        return luasService.fetchStillorganTowardsInbound()
    }
}