package dev.markos.template.data.api

import dev.markos.template.model.StopInfo
import io.reactivex.Single
import retrofit2.http.GET

interface LuasService {

    @GET("xml/get.ashx?action=forecast&stop=mar&encrypt=false")
    fun fetchMarlboroughTowardsOutbound(): Single<StopInfo>

    @GET("xml/get.ashx?action=forecast&stop=sti&encrypt=false")
    fun fetchStillorganTowardsInbound(): Single<StopInfo>

    companion object {
        const val BASE_URL = "https://luasforecasts.rpa.ie/"
    }
}