package dev.markos.template.data

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {
    val TAG = this::class.java.simpleName

    val disposibles = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposibles.clear()
    }
}