package dev.markos.template.model

import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Xml

// <direction name="Outbound">
// <tram dueMins="2" destination="Sandyford" />
// <tram dueMins="8" destination="Bride's Glen" />
// <tram dueMins="13" destination="Sandyford" />
// <tram dueMins="16" destination="Bride's Glen" />
// </direction>
@Xml
data class Direction(
    @Attribute(name = "name") val name : String,
    @Element(name = "tram") val trams: List<Tram>
)