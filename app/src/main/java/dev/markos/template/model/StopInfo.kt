package dev.markos.template.model

import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "stopInfo")
data class StopInfo(
    @Attribute(name = "created") val createdDate : String,
    @Attribute(name = "stop") val stop : String,
    @Attribute(name = "stopAbv") val stopAbv: String,
    @Element(name = "direction") val directions: List<Direction>
)