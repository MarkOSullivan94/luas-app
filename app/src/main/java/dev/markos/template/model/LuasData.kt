package dev.markos.template.model

data class LuasData (
    val trams: List<Tram>
)