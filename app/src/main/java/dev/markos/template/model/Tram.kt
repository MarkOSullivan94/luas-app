package dev.markos.template.model

import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Xml

// <tram dueMins="2" destination="Sandyford" />
@Xml
data class Tram(
    @Attribute(name = "dueMins") val dueMins : String,
    @Attribute(name = "destination") val destination: String
)