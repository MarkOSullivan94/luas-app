package dev.markos.template.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.markos.template.R
import dev.markos.template.databinding.TramItemBinding
import dev.markos.template.model.Tram

class TramListAdapter : ListAdapter<Tram, TramListAdapter.TramViewHolder>(TramDiffCallback) {

    class TramViewHolder(binding: TramItemBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.tramItemContainer) {

        private var tram: Tram? = null
        private val destination = binding.destinationValue
        private val time = binding.timeValue

        fun bind(tram: Tram) {
            this.tram = tram
            destination.text = tram.destination
            time.text = when (tram.dueMins.toLowerCase()) {
                "due" -> context.getString(R.string.any_minute)
                else -> context.getString(R.string.due_minutes, tram.dueMins)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TramViewHolder {
        val binding = TramItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TramViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: TramViewHolder, position: Int) {
        val tram = getItem(position)
        holder.bind(tram)
    }
}

object TramDiffCallback : DiffUtil.ItemCallback<Tram>() {
    override fun areItemsTheSame(oldItem: Tram, newItem: Tram): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Tram, newItem: Tram): Boolean {
        return oldItem.dueMins == newItem.dueMins
    }
}