package dev.markos.template

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.constraintlayout.widget.Group
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import dev.markos.template.data.LuasViewModel
import dev.markos.template.data.viewstate.LuasViewState
import dev.markos.template.databinding.ActivityMainBinding
import dev.markos.template.list.TramListAdapter

@AndroidEntryPoint
class MainActivity : BaseActivity<LuasViewModel, ActivityMainBinding>() {

    override val viewModel: LuasViewModel by viewModels()
    private lateinit var nextDue: TextView
    private lateinit var to: TextView
    private lateinit var refresh: ImageView
    private lateinit var readyGroup: Group
    private lateinit var errorGroup: Group
    private lateinit var list: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nextDue = binding.nextDueValue
        to = binding.toValue
        refresh = binding.refresh
        readyGroup = binding.readyGroup
        errorGroup = binding.errorGroup
        list = binding.list

        viewModel.luasForecast.observe(this, {
            if (it is LuasViewState.Ready) {
                updateDetails(it)
                showList(it)
            } else {
                showError()
            }
        })

        refresh.setOnClickListener {
            viewModel.updateForecast()
        }
    }

    private fun updateDetails(state: LuasViewState.Ready) {
        if (!readyGroup.isVisible) readyGroup.isVisible = true
        val next = state.data.trams.first()

        nextDue.text = when (next.dueMins.toLowerCase()) {
            "due" -> getString(R.string.any_minute)
            else -> getString(R.string.due_minutes, next.dueMins)
        }

        to.text = next.destination
    }

    private fun showList(state: LuasViewState.Ready) {
        val adapter = TramListAdapter()
        list.adapter = adapter

        // don't need first tram details as we are already showing it
        val modifiedList = state.data.trams - state.data.trams.first()
        adapter.submitList(modifiedList)
    }

    private fun showError() {
        if (!errorGroup.isVisible) errorGroup.isVisible = true
    }

    override fun setupViewBinding() = ActivityMainBinding.inflate(layoutInflater)
}