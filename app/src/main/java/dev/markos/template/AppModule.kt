package dev.markos.template

import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.converter.htmlescape.HtmlEscapeStringConverter
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dev.markos.template.data.api.LuasService
import dev.markos.template.data.api.LuasService.Companion.BASE_URL
import dev.markos.template.data.datahelper.BaseDataHelper
import dev.markos.template.data.datahelper.DataHelper
import dev.markos.template.data.direction.DefaultLuasDirectionHelper
import dev.markos.template.data.direction.LuasDirectionHelper
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class AppModule {

    @Singleton
    @Provides
    fun providesLuasService(): LuasService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(
            TikXmlConverterFactory.create(
                TikXml.Builder()
                    .exceptionOnUnreadXml(false)
                    .addTypeConverter(String::class.java, HtmlEscapeStringConverter())
                    .build()
            )
        )
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(LuasService::class.java)

    @Singleton
    @Provides
    fun provideDirectionHelper(): LuasDirectionHelper {
        return DefaultLuasDirectionHelper
    }

    @Singleton
    @Provides
    fun provideDataHelper(): BaseDataHelper {
        return DataHelper
    }
}
