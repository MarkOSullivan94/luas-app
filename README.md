# LUAS App

Libraries used include:

* Retrofit2 - Networking requests
* RxJava2 - Asynchronious requests & observable streams
* ConstraintLayout - Layouts
* Tikxml - Parsing XML API response
* ViewModel - Storing & managing UI related data
* LiveData - Lifecycle aware observable data
* Hilt - Dependency injection
* JUnit4 - Unit Testing

Also used:

* ViewBinding (Experimental but thought it'd be fun trying it out) - Code for easily interacting with views
